#!/usr/bin/env python

import rospy
import numpy as np
from sensor_msgs.msg import CameraInfo
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import copy


class CameraClass:
    def __init__(self, camera_namespace="dynamic_objects", camera_name="camera", info_name="info_image", raw_rgb_img="raw_image", altitude=1.335 ):

        self.altitude = altitude
        self.camera_info_topic = "/" + camera_namespace + "/" + camera_name + "/" + info_name
        self.camera_img_topic = "/" + camera_namespace + "/" + camera_name + "/" + raw_rgb_img
        self.bridge = CvBridge()

        self._check_camera_ready()
        self.listener_camera_info = rospy.Subscriber(self.camera_info_topic, CameraInfo, self.callbackCameraInfo)
        self.listener_image = rospy.Subscriber(self.camera_img_topic, Image, self.callbackImg)

    def _check_camera_ready(self):
        self._check_camera_info_ready()
        self._check_camera_img_ready()
        rospy.logdebug("Camera Ready...")

    def _check_camera_info_ready(self):
        self.camerainfo = None
        rospy.logdebug("Waiting for "+self.camera_info_topic+" to be READY...")
        while self.camerainfo is None and not rospy.is_shutdown():
            try:
                self.camerainfo = rospy.wait_for_message(self.camera_info_topic, CameraInfo, timeout=5.0)
                rospy.logdebug("Current "+self.camera_info_topic+" READY=>")
            except:
                rospy.logerr(self.camera_info_topic+" not ready yet, retrying")

    def _check_camera_img_ready(self):
        self.image = None
        rospy.logdebug("Waiting for " + self.camera_img_topic + " to be READY...")
        while self.image is None and not rospy.is_shutdown():
            try:
                self.image = self.ros2np(rospy.wait_for_message(self.camera_img_topic, Image, timeout=5.0))
                rospy.logdebug("Current " + self.camera_img_topic + " READY=>")
            except Exception as e:
                s = str(e)
                rospy.loginfo("Error, The _robots_models_dict is not ready = " + s)
                rospy.logerr(self.camera_img_topic + " not ready yet, retrying")
                

    def ros2np(self, img):
        return self.bridge.imgmsg_to_cv2(img, "bgr8")

    def callbackImg(self, image):
        self.image = self.ros2np(image)

    def callbackCameraInfo(self, camerainfo):
        self.camerainfo = camerainfo
        if self.camerainfo is not None:
            self.K = np.matrix(np.reshape(self.camerainfo.K, (3, 3)))
            self.P = np.matrix(np.reshape(self.camerainfo.P, (3, 4)))

    def getImage(self):
        if self.image is not None:
            return copy.deepcopy(self.image)
        else:
            return None

    def get_camera_info(self):
        if self.camerainfo is not None:
            return copy.deepcopy(self.camerainfo)
        else:
            return None