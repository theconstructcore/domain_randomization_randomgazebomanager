#!/usr/bin/env python

import os
import sys
import shutil

class GenFilesTester(object):

    def __init__(self, images_path = ".", images_anotations_path = "."):

        self._images_path = images_path
        self._images_anotations_path = images_anotations_path

        self.get_files_arrays()

    def get_files_arrays(self, print_names=False):
        """
        Generate the arrays that will be used to after make checks and operations
        :param print_names:
        :return:
        """
        # Check Images
        self._images_array = []
        for root, dirs, files in os.walk(self._images_path):
            for filename in files:
                name = os.path.splitext(filename)[0]
                self._images_array.append(name)
                if print_names:
                    print(filename)

        # Check Images Annotations
        self._annotations_array = []
        for root, dirs, files in os.walk(self._images_anotations_path):
            for filename in files:
                name = os.path.splitext(filename)[0]
                self._annotations_array.append(name)
                if print_names:
                    print(filename)


    def check_one_to_one_corrrespondance(self, clean_orphan_files=False, print_names=False):
        """
        We Check that for each image there is its corresponding annotation
        :param remove_orphan_files:
        :return:
        """

        self.coupled_files_array = []

        num_images_files = len(self._images_array)
        num_annotations_files = len(self._annotations_array)

        index = 0
        for image_file_name in self._images_array:

            for annotation_file_name in self._annotations_array:
                if image_file_name == annotation_file_name:
                    if image_file_name not in self.coupled_files_array:
                        self.coupled_files_array.append(image_file_name)
                        #print("FOUND COUPLE="+str(image_file_name)+","+str(annotation_file_name))
                    else:
                        print("Name Already in List")

            self.update_progress(float(index)/float(num_images_files), text=image_file_name)
            index += 1

        num_coupled_files = len(self.coupled_files_array)
        print("Files Images = "+str(num_images_files))
        print("Files Annotations = " + str(num_annotations_files))
        print("Files Coupled = " + str(num_coupled_files))

        if num_coupled_files == num_images_files and num_coupled_files == num_annotations_files:
            print('\x1b[6;30;42m' + 'Everything OK' + '\x1b[0m')
        else:
            print('\x1b[0;37;41m' + 'Some Files are ORPHANS' + '\x1b[0m')
            if clean_orphan_files:
                self.clean_orphans(self.coupled_files_array)


    def clean_orphans(self, coupled_files_array):
        """
        We clean all te files that AREN'T in the coupled_files_array
        :param coupled_files_array:
        :return:
        """

        # We go Over the files inside the Image Folder, create clean folder and copy the ones in list:
        path_clean_images_path = os.path.join(self._images_path,"../images_clean")
        if os.path.exists(path_clean_images_path):
            print("Cleaning UP..." + str(path_clean_images_path))
            shutil.rmtree(path_clean_images_path)

        os.makedirs(path_clean_images_path)

        path_clean_annotations_path = os.path.join(self._images_anotations_path,"../dataset_gen_annotations_clean")
        if os.path.exists(path_clean_annotations_path):
            print("Cleaning UP..." + str(path_clean_annotations_path))
            shutil.rmtree(path_clean_annotations_path)
        os.makedirs(path_clean_annotations_path)

        index = 0
        num_couple_img = len(coupled_files_array)
        for file_name in coupled_files_array:
            image_name = file_name+".png"
            annotation_name = file_name + ".xml"

            image_name_path = os.path.join(self._images_path, image_name)
            image_name_new_path = os.path.join(path_clean_images_path,image_name)

            annotations_name_path = os.path.join(self._images_anotations_path, annotation_name)
            annotations_name_new_path = os.path.join(path_clean_annotations_path, annotation_name)


            shutil.copyfile(image_name_path, image_name_new_path)
            shutil.copyfile(annotations_name_path, annotations_name_new_path)

            #print("Couple Files File Copied!")
            image_file_name = "Doing Copy Of file="+str(file_name)
            self.update_progress(float(index)/float(num_couple_img), text=image_file_name)
            index += 1

    def update_progress(self, progress, text="Percent"):
        barLength = 10 # Modify this to change the length of the progress bar
        status = ""
        if isinstance(progress, int):
            progress = float(progress)
        if not isinstance(progress, float):
            progress = 0
            status = "error: progress var must be float\r\n"
        if progress < 0:
            progress = 0
            status = "Halt...\r\n"
        if progress >= 1:
            progress = 1
            status = "Done...\r\n"
        block = int(round(barLength*progress))
        text = "\r"+text+": [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
        sys.stdout.write(text)
        sys.stdout.flush()




if __name__ == "__main__":


    if len(sys.argv) < 2:
        print("usage: gen_files_tester.py dataset_path True")
    else:

        print(str(sys.argv))

        dataset_path = sys.argv[1]
        clean_orphan_files = bool(sys.argv[2] == "True")

        #dataset_path = "/home/rdaneel/playground/tiago_randomenv_real_miguel_1/tiago_randomenv_real_miguel"
        images_path = os.path.join(dataset_path, "dataset_gen/images")
        images_anotations_path = os.path.join(dataset_path, "dataset_gen_annotations")

        gen_files_tester_obj = GenFilesTester(images_path,
                                              images_anotations_path)

        gen_files_tester_obj.check_one_to_one_corrrespondance(clean_orphan_files=clean_orphan_files)


        if clean_orphan_files:
            ## Check new path
            images_path = os.path.join(dataset_path, "dataset_gen/images_clean")
            images_anotations_path = os.path.join(dataset_path, "dataset_gen_annotations_clean")

            gen_files_tester_obj = GenFilesTester(images_path,
                                                images_anotations_path)

            gen_files_tester_obj.check_one_to_one_corrrespondance(clean_orphan_files=False)





