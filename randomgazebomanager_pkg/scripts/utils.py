import numpy as np
import cv2

def reframing(x) :
	outy = []
	for elx in x :
		y = np.zeros_like(elx)
		#Z axis : forward <--> x
		y[2,0] = elx[0,0]
		#X axis : right <--> minus y
		y[0,0] = -elx[1,0]
		#Y axis : below <--> minus z 
		y[1,0] = -elx[2,0]
		#y[1,0] = -0.2
		outy.append(y)
	return outy
	
def makeboundingbox(x,size=0.2) :
	y1 = np.copy(x)
	y2 = np.copy(x)
	'''
	y1[0,0] -= 3*size
	y2[0,0] += 3*size
	y1[1,0] -= 3*size
	y2[1,0] += size
	y1[2,0] += 2*size
	y2[2,0] += 2*size
	'''
	y1[0,0] -= 2*size
	y2[0,0] += 2*size
	y1[1,0] -= 2*size
	y2[1,0] += 2*size
	y1[2,0] += 1*size
	y2[2,0] += 1*size
	return y1,y2